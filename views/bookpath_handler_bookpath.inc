<?php

// This file must be at mymodule/views directory.

/**
 * @file
 * Definition of mymodule_handler_handlername.
 */

/**
 * Description of what my handler does.
 */
class bookpath_handler_bookpath extends views_handler_field {
  /**
   * Add some required fields needed on render().
   */
  function construct() {
    parent::construct();
  }

  /**
   * Loads additional fields.
   */
  function query() {

  }

  /**
   * Default options form.
   */
  function option_definition() {
    $options = parent::option_definition();

    $options['option_seperator'] = array('default' => ' >> ');
    $options['option_link'] = array('default' => TRUE);

    return $options;
  }

  /**
   * Creates the form item for the options added.
   */
  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);

    $form['option_seperator'] = array(
      '#type' => 'textfield',
      '#title' => t('Label'),
      '#default_value' => $this->options['option_seperator'],
      '#description' => t('Enter the seperator between parents'),
      '#weight' => -10,
    );

	  $options = array(0 => t('No'), 1 => t('Yes'));
    $form['option_link'] = array(
      '#type' => 'radios',
      '#title' => t('Link'),
      '#default_value' => $this->options['option_link'],
	    '#options' => $options,
      '#description' => t('Select if you need it as link'),
      '#weight' => -9,
    );
  }

  /**
   * Renders the field handler.
   */
  function render($values) {
  	return bookpath_get_path($values->nid, $this->options['option_link'], $this->options['option_seperator']);
  }
}
