<?php
/**
 * @file
 * Views definitions for mymodule module.
 */

/**
 * Implementation of hook_views_handlers() to register all of the basic handlers
 * views uses.
 */
function bookpath_views_handlers() {
  return array(
    'info' => array(
      'path' => drupal_get_path('module', 'bookpath'),
    ),
    'handlers' => array(
      // The name of my handler
      'bookpath_handler_bookpath' => array(
        // The name of the handler we are extending.
        'parent' => 'views_handler_field',
      ),
    ),
  );
}

/**
 * Implements hook_views_data().
 */
function bookpath_views_data() {
  $data = array();
  $data['node']['bookpath'] = array(
    'title' => t('Book path'),
    'help' => t('Book as breadcrumb.'),
    'field' => array(
      'handler' => 'bookpath_handler_bookpath',
    ),
  );
  return $data;
}
